class Node:
    def __init__(self, val, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

def dfs(root, arr=[]):
    if root:
        arr.append(root.val)
        dfs(root.left)
        dfs(root.right)
        return arr

def bfs(arr, i=0):
    if arr[i].left:
        arr.append(arr[i].left)
    if arr[i].right:
        arr.append(arr[i].right)
    if len(arr) - 1 > i:
        i += 1
        return bfs(arr, i)
    return arr