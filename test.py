from main import Node, dfs, bfs

d = Node(4)
e = Node(5)
f = Node(6)
g = Node(7)
b = Node(2, d, e)
c = Node(3, f, g)
a = Node(1, b, c)

def dfs_test():
    if dfs(a) == [1, 2, 4, 5, 3, 6, 7]:
        print('PASS')
    else:
        print('FAIL')

def bfs_test():
    arr = [i.val for i in bfs([a])]
    if arr == [1, 2, 3, 4, 5, 6, 7]:
        print('PASS')
    else:
        print('FAIL')

dfs_test()
bfs_test()